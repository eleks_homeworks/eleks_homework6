﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Homework3.Implementations;
using Homework3.Interfaces;
using Homework3.Models;
using Newtonsoft.Json;
using Ninject;

namespace Homework3
{
    static class Program
    {
        private const string FileName = "library.json";

        static void Main()
        {
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            var helper = new JsonSerializerHelper();
            var library = new Library {Helper = helper};
            library.CreateLibrary();

            Console.WriteLine(library);

            Console.WriteLine("Start of serialization");
            Console.WriteLine(library.Serialize(FileName));
            Console.WriteLine($"Library serialized in file \"{FileName}\"");

            Console.WriteLine("Start of deserialization");
            var deserializedLibrary = library.Deserialize(FileName);
            Console.WriteLine($"Library deserialized from file \"{FileName}\"");
            Console.WriteLine(deserializedLibrary);

            Console.Read();
        }
    }
}
