﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;
using Homework3.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ninject;
using Ninject.Syntax;

namespace Homework3.Implementations
{
    public class JsonSerializerHelper : ISerializerHelper
    {
        public string SerializeLibrary(string fileName, Library library)
        {
            var jsonString = JsonConvert.SerializeObject(library);

            using (var writer = File.CreateText(fileName))
            {
                writer.Write(jsonString);
            }
            
            return jsonString;
        }

        public Library DeserializeLibrary(string fileName)
        {
            string jsonString;
            using (var reader = File.OpenText(fileName))
            {
                jsonString = reader.ReadToEnd();
            }
            var instance = JsonConvert.DeserializeObject<Library>(jsonString);
            return instance;
        }
    }
}
