﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3.Interfaces
{
    public interface ICountingBooks
    {
        int GetBooksCount();
    }
}
