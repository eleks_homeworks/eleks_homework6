﻿using Homework3.Models;
using Newtonsoft.Json;

namespace Homework3.Interfaces
{
    public interface ISerializerHelper
    {
        string SerializeLibrary(string fileName, Library library);
        Library DeserializeLibrary(string fileName);
    }
}