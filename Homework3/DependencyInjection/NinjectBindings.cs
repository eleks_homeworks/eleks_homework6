﻿using Homework3.Implementations;
using Homework3.Interfaces;
using Homework3.Models;

namespace Homework3.DependencyInjection
{
    public class NinjectBindings : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<ISerializerHelper>().To<JsonSerializerHelper>();
        }
    }
}
