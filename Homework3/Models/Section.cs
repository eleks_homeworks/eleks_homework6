﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Section : IComparable<Section>, ICountingBooks
    {
        private List<Author> _authors;

        public string Type { get; set; }

        public List<Author> Authors
        {
            get { return _authors ?? (_authors = new List<Author>()); }
            set { _authors = value; }
        }

        public Section() { }

        public Section(string type, List<Author> authors)
        {
            Type = type;
            Authors = authors;
        }

        public int CompareTo(Section section)
        {
            if (GetBooksCount() > section.GetBooksCount())
            {
                return 1;
            }
            if (GetBooksCount() < section.GetBooksCount())
            {
                return -1;
            }
            return 0;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append($"The \"{Type}\" section has {GetBooksCount()} books total\n\n");

            var sortedAuthors = Authors.OrderBy(a => a.GetBooksCount());

            foreach (var a in sortedAuthors)
            {
                builder.Append($"{a}\n");
            }
            return builder.ToString();
        }

        public int GetBooksCount() => Authors.Sum(a => a.GetBooksCount());
    }
}
