﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Book : IComparable<Book>
    {
        public string Title { get; set; }
        public int Pages { get; set; }

        public Book() { }

        public Book(string title, int pages)
        {
            Title = title;
            Pages = pages;
        }

        public int CompareTo(Book book)
        {
            if (Pages > book.Pages)
            {
                return 1;
            }
            if (Pages < book.Pages)
            {
                return -1;
            }
            return 0;
        }

        public override string ToString() => $"\"{Title}\" with {Pages} pages";
    }
}
