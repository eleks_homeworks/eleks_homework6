﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Implementations;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Library : IComparable<Library>, ICountingBooks
    {
        private List<Section> _sections;
        private ISerializerHelper helper;

        public ISerializerHelper Helper
        {
            private get { return helper ?? (helper = new JsonSerializerHelper()); }
            set { helper = value; }
        }

        public List<Section> Sections
        {
            get { return _sections ?? (_sections = new List<Section>()); }
            set { _sections = value; }
        }

        public Library() {}

        public Library(ISerializerHelper helper, List<Section> sections)
        {
            Helper = helper;
            Sections = sections;
        }

        public int CompareTo(Library library)
        {
            if (GetBooksCount() > library.GetBooksCount())
            {
                return 1;
            }
            if (GetBooksCount() < library.GetBooksCount())
            {
                return -1;
            }
            return 0;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append($"The library has {GetBooksCount()} books total\n\n");

            var sortedSections = Sections.OrderBy(s => s.GetBooksCount());

            foreach (var s in sortedSections)
            {
                builder.Append($"{s}\n");
            }
            return builder.ToString();
        }

        public int GetBooksCount() => Sections.Sum(s => s.GetBooksCount());

        public string Serialize(string fileName) => Helper.SerializeLibrary(fileName, this);

        public Library Deserialize(string fileName) => Helper.DeserializeLibrary(fileName);

        // Additional tasks

        public void ShowBiggestSection()
        {
            var maxCount = Sections.Max(s => s.GetBooksCount());
            var biggestSection = Sections.FirstOrDefault(s => s.GetBooksCount() == maxCount);

            Console.WriteLine(biggestSection == null ? "Error" :
                $"The biggest section is {biggestSection.Type}: {biggestSection.GetBooksCount()} books");
        }

        public void ShowHardworkingAuthor()
        {
            var authors = Sections.SelectMany(s => s.Authors);
            var maxCount = authors.Max(a => a.GetBooksCount());
            var hardworkingAuthor = authors.FirstOrDefault(a => a.GetBooksCount() == maxCount);

            Console.WriteLine(hardworkingAuthor == null ? "Error" :
                $"The hardworking author is {hardworkingAuthor.Name}: {hardworkingAuthor.GetBooksCount()} books");
        }

        public void ShowThinnestBook()
        {
            var authors = Sections.SelectMany(s => s.Authors);
            var books = authors.SelectMany(a => a.Books);
            var minPagesCount = books.Min(b => b.Pages);
            var thinnestBook = books.FirstOrDefault(b => b.Pages == minPagesCount);

            Console.WriteLine(thinnestBook == null ? "Error" :
                $"The thinnest book is {thinnestBook.Title}: {thinnestBook.Pages} pages");
        }

        public void CreateLibrary()
        {
            var stehpenKing = new Author("Stephen King", new List<Book>
            {
                new Book("11/22/63", 894),
                new Book("It", 1340),
                new Book("Joyland", 317),
                new Book("Revival", 413)
            });

            var maxKidruk = new Author("Max Kidruk", new List<Book>
            {
                new Book("Bot", 434),
                new Book("Stronghold", 390)
            });

            var thrillers = new Section("Thillers", new List<Author>
            {
                stehpenKing,
                maxKidruk
            });

            var jeffSutherland = new Author("Jeff Sutherland", new List<Book>
            {
                new Book("Scrum", 278)
            });

            var jeffreyRichter = new Author("Jeffrey Richter", new List<Book>
            {
                new Book("CLR via C#", 895)
            });

            var steveMcConnell = new Author("Steve McConnell", new List<Book>
            {
                new Book("Code complete", 880)
            });

            var education = new Section("Education", new List<Author>
            {
                jeffSutherland,
                jeffreyRichter,
                steveMcConnell
            });

            Sections.Add(thrillers);
            Sections.Add(education);
        }
    }
}
