﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Author : IComparable<Author>, ICountingBooks
    {
        private List<Book> _books;

        public string Name { get; set; }

        public List<Book> Books
        {
            get { return _books ?? (_books = new List<Book>()); }
            set { _books = value; }
        }

        public Author() { }

        public Author(string name, List<Book> books)
        {
            Name = name;
            Books = books;
        }

        public int CompareTo(Author author)
        {
            if (GetBooksCount() > author.GetBooksCount())
            {
                return 1;
            }
            if (GetBooksCount() < author.GetBooksCount())
            {
                return -1;
            }
            return 0;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append($"{Name} has written {Books.Count} books\n");

            var sortedBooks = Books.OrderBy(b => b.Pages);

            foreach (var b in sortedBooks)
            {
                builder.Append($"{b}\n");
            }
            return builder.ToString();
        }


        public int GetBooksCount() => _books.Count;
    }
}
