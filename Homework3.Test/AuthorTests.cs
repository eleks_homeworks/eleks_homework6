﻿using System;
using System.Collections.Generic;
using Moq;
using Homework3.Interfaces;
using Homework3.Models;
using NUnit.Framework;

namespace Homework3.Test
{
    [TestFixture]
    public class AuthorTests
    {
        Book book = new Book("book", 123);

        [Test]
        public void CompareTo_AuthorIsNull_ThrowsNullReferenceException()
        {
            var author = new Author("test", new List<Book> { book });

            Assert.Throws<NullReferenceException>(() => author.CompareTo(null));
        }

        [Test]
        public void CompareTo_EqualsAuthors_ReturnEquals()
        {
            var author = new Author("test", new List<Book> {book});
            Assert.AreEqual(0, author.CompareTo(author));
        }

        [Test]
        public void CompareTo_AuthorIsGreater_ReturnGreater()
        {
            var author = new Author("test", new List<Book> { book, book });
            var someAuthor = new Author("test", new List<Book> { book });
            Assert.AreEqual(1, author.CompareTo(someAuthor));
        }

        [Test]
        public void CompareTo_AuthorIsLess_ReturnLess()
        {
            var author = new Author("test", new List<Book> { book });
            var someAuthor = new Author("test", new List<Book> { book, book });
            Assert.AreEqual(-1, author.CompareTo(someAuthor));
        }

        [Test]
        public void GetBooksCount_HasTwoBooks_ReturnTwo()
        {
            var author = new Author("test", new List<Book> { book, book });
            Assert.AreEqual(2, author.GetBooksCount());
        }

        [Test]
        public void ToString_HasOneBook_ReturnString()
        {var author = new Author("author", new List<Book> { book });
            var expectedString = "author has written 1 books\nbook with 123 pages\n";
            Assert.AreEqual(expectedString, author.ToString());
        }
    }
}
