﻿using System;
using System.Collections.Generic;
using System.IO;
using Homework3.Implementations;
using Homework3.Interfaces;
using Homework3.Models;
using NUnit.Framework;
using Moq;

namespace Homework3.Test
{
    [TestFixture]
    class LibraryTests
    {
        [Test]
        public void Serialize_HasLibrary_ResultJsonString()
        {
            const string expected = "serialized";
            const string fileName = "test.json";

            Mock<ISerializerHelper> helperMock = new Mock<ISerializerHelper>();
            helperMock.Setup(h => h.SerializeLibrary(It.IsAny<string>(), It.IsAny<Library>())).Returns(expected);

            var book = new Book("book", 123);
            var author = new Author("author", new List<Book> { book });
            var section = new Section("section", new List<Author> { author });
            var library = new Library(helperMock.Object, new List<Section> { section });
            
            var jsonString = library.Serialize(fileName);

            Assert.AreEqual(expected, jsonString);
        }

        [Test]
        public void Serialize_FileNameIsNull_ResultJsonString()
        {
            Mock<ISerializerHelper> helperMock = new Mock<ISerializerHelper>();
            helperMock.Setup(h => h.SerializeLibrary(null, It.IsAny<Library>())).Throws<NullReferenceException>();

            var book = new Book("book", 123);
            var author = new Author("author", new List<Book> { book });
            var section = new Section("section", new List<Author> { author });
            var library = new Library(helperMock.Object, new List<Section> { section });
            
            Assert.Throws<NullReferenceException>(() => library.Serialize(null));
        }
    }
}
